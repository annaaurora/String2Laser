/*
  String2Laser - Library for producing binary power signals with an Arduino from strings or key value pairs out of strings. Those signals can e.g. power a laser or LED to transmit the input to another device. Created by Anna Aurora Neugebauer, 2023-01-16.
*/
#define Morse_h

#include "Arduino.h"

class String2Laser
{
  public:
    String2Laser(unsigned int laserPin, unsigned int timeMilsecs);
    void laserWrite(bool bit);
    void laserWriteString(String string);
    String padString(String string);
    void laserWritePaddedString(String string);
    String keyValueString(String key, String value);
  private:
    int _laserPin;
    int _timeMilsecs;
};
