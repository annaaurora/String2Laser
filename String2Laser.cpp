/*
  String2Laser - Library for producing binary power signals with an Arduino from strings or key value pairs out of strings. Those signals can e.g. power a laser or LED to transmit the input to another device. Created by Anna Aurora Neugebauer, 2023-01-16.
*/
#include "Arduino.h"
#include "String2Laser.h"

String2Laser::String2Laser(unsigned int laserPin, unsigned int timeMilsecs) {
  _laserPin = laserPin;
  _timeMilsecs = timeMilsecs;
  pinMode(_laserPin, OUTPUT);
  digitalWrite(_laserPin, LOW);
}

void String2Laser::laserWrite(bool bit) {
  if (bit) {
    digitalWrite(_laserPin, HIGH);
  } else {
    digitalWrite(_laserPin, LOW);
  }
}

// Write string with laser
void String2Laser::laserWriteString(String string) {
  // Start sequence to transmit bit duration and that a string will need to be read.
  Serial.println("Writing start sequence…")
  String2Laser::laserWrite(true);
  delay(_timeMilsecs);
  String2Laser::laserWrite(false);

  // For every character in string
  for(int i=0;i<string.length(); i++){
    char character = string.charAt(i);
    Serial.print(character);

    // For every bit of character
    for(int i=7; i>=0; i--){
      bool bit = bitRead(character,i);
      // bit is an integer but you need a boolean to laserWrite()
      if (bit==1) {
        String2Laser::laserWrite(true);
      } else {
        String2Laser::laserWrite(false);
      }
      // Wait 1 µs
      //delayMicroseconds(1);
      // Wait input time in ms
      delay(_timeMilsecs);
      Serial.print(bit);
    }

    Serial.println();
  }

  // Turn off laser regardless if last bit was true or false
  digitalWrite(_laserPin, LOW);
}

// Pad string with separators.
// Seprarators are necessary to seperate key value pairs. If mothership would just get sent key value pair after key value pair it would think that "valuekey" is a value and they get confused about the ":" after that.
// String input format: "key:value"
// String output: "key:value;"
String String2Laser::padString(String string) {
  string.concat(";");
  return string;
}

// Write padded string with laser
void String2Laser::laserWritePaddedString(String string) {
  Serial.println("To be padded string:");
  Serial.println(string);
  string = String2Laser::padString(string);
  Serial.println("Padded string:");
  Serial.println(string);
  String2Laser::laserWriteString(string);
}

String String2Laser::keyValueString(String key, String value) {
    String string = key;
    string.concat(":");
    string.concat(value);
    return string;
}
